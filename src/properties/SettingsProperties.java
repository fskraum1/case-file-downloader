/*
 * Application is intended to be used by GMC Support department for downloading 
 * files from DataPoint
 * Copyright (C) 2013  Miroslav Kraus

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License    
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package properties;

import gui.DialogCredentials;
import java.awt.Rectangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import main.CasefileDownloader;
import org.apache.log4j.Logger;

/**
 *
 * @author m.kraus
 */
public class SettingsProperties {
    private static volatile SettingsProperties singleton;
    private static Properties propertiesSaved = new Properties();   //properties which will be saved to disk
    private static Properties propertiesNotSaved = new Properties();    //properties which will not be stored to disk and thus will be default after application start
    static Logger log = Logger.getLogger(SettingsProperties.class.getName());
        
    //defaults
    private static final String propertyFileName = "settings.properties";
    private static final String defaultHostNameDataPoint = "datapoint.gmc.net";
    private static final String defaultPortDataPoint = "822";
    private static final String defaultLocalPathRoot = "N:\\Support\\HelpDesk\\SF\\";
    private static final String defaultDataPointRootsList = "/Customers All/EMEA/!SUPPORT|/Customers All/AMERICA/!SUPPORT|/Customers All/ASIA/!SUPPORT";
            
    //property names
    private static final String PROPERTY_HOST_NAME = "propertyHostName";
    private static final String PROPERTY_PORT = "propertyPort";
    private static final String PROPERTY_LOCAL_ROOT_PATH = "propertyLocalRootPath";
    private static final String PROPERTY_REMOTE_ROOTS_LIST = "propertyRemoteRootsList";
    private static final String PROPERTY_LAST_UPLOAD_DIR = "propertyLastUploadDir";
    private static final String PROPERTY_FRAME_POSITION = "propertyFramePosition";

    
    
    /**
     * Settings properties instance is always singleton so this method is the 
     * only way how to get the instance of the SettingsProperties object. In case
     * the instance does not exists yet it is created otherwise the reference to
     * it is provided
     * @return singleton instance of SettingsProperties object
     */
    public static SettingsProperties getInstance(){
        if(singleton==null){
            synchronized(SettingsProperties.class){
                if (singleton == null) {
                    singleton = new SettingsProperties();
                }    
            }            
        }
        return singleton;
    }
    
    /**
     *  Creates new SettingsProperties object. In case property file name is not 
     *  found on the disk, default values are used and Username and Password
     *  dialog is shown to the user to obtain credentials. These properties are 
     *  are than saved on disk.
     */
    private SettingsProperties() {
        File propertyFile = new File(propertyFileName);
        if (propertyFile.exists()) {
            readPropertiesFromFile();
        }
        else {
            propertiesSaved.put(PROPERTY_HOST_NAME, defaultHostNameDataPoint);
            propertiesSaved.put(PROPERTY_PORT, defaultPortDataPoint);
            propertiesSaved.put(PROPERTY_LOCAL_ROOT_PATH, defaultLocalPathRoot);
            propertiesSaved.put(PROPERTY_REMOTE_ROOTS_LIST, defaultDataPointRootsList);
            writePropertiesToFile();
        }
        propertiesNotSaved.put(PROPERTY_LAST_UPLOAD_DIR, defaultLocalPathRoot);

    }
            

    /**
     * if credentials are not set in properties yet returns true
     * @return returns true if either password or userName are not set yet in the 
     * properties
     */
    public Boolean isCredentialsEmpty(){
        if(propertiesSaved.getProperty("password")==null || propertiesSaved.getProperty("userName")==null){
            return true;
        }
        else
            return false;
            
        
    }
    
    /**
     *
     * Returns Password from the stored properties. In case password is not in 
     * the properties, credentials dialog is initiated
     * @return Password from the stored properties
     */
    public String getPassword() {
        if(propertiesSaved.getProperty("password")==null){
            DialogCredentials dc = new DialogCredentials(CasefileDownloader.getFrame(), CasefileDownloader.getProgressContainer());
            dc.setLocationRelativeTo(CasefileDownloader.getFrame());
            dc.setVisible(true);
            return getPassword();
        } else {
            return propertiesSaved.getProperty("password");
        }
    }

    /**
     * Returns port for DataPoint from the stored properties
     * @return port for DataPoint from the stored properties
     */
    public int getPortDataPoint() {
        return Integer.valueOf(propertiesSaved.getProperty(PROPERTY_PORT));
    }

    /**
     * Returns hostname for DataPoint from the stored properties
     * @return hostname for DataPoint from the stored properties
     */
    public String getHostNameDataPoint() {
        return propertiesSaved.getProperty(PROPERTY_HOST_NAME);
    }

    /**
     * Returns username for DataPoint from the stored properties. In case username
     * is not in the properties, credentials dialog is initiated
     * @return username for DataPoint from the stored properties
     */
    public String getUsername() {
        if(propertiesSaved.getProperty("userName")==null){
            DialogCredentials dc = new DialogCredentials(CasefileDownloader.getFrame(),CasefileDownloader.getProgressContainer());
            dc.setLocationRelativeTo(CasefileDownloader.getFrame());
            dc.setVisible(true);
            return getUsername();
        } else {
            return propertiesSaved.getProperty("userName");
        }
    }
    
    /**
     * Returns local path where the downloaded files will be stored
     * from the stored properties
     * @return local path where the downloaded files will be stored
     */
    public String getLocalPathRoot(){
        return propertiesSaved.getProperty(PROPERTY_LOCAL_ROOT_PATH);
    }
    
    /**
     * Returns String of the root directories on DP where it is necessary
     * to look for the directory with the current ticket ID
     * TODO> should be changed to return String[] I think almost everywhere is 
     * this used as a String[] just after call - search for 
     * @return String delimited by "|" containing root directories on DP
     * 
     */
    public String getDataPointRootsList(){
        return propertiesSaved.getProperty(PROPERTY_REMOTE_ROOTS_LIST);
    }
    
    /**
     * Returns File object representing last location of the upload filechooser
     * 
     * @return File object representing last location of the upload filechooser
     * 
     */
    public File getLastUploadDirectory(){
        return new File(propertiesNotSaved.getProperty(PROPERTY_LAST_UPLOAD_DIR));
    }
    
    /**
     * 
     * @return name of the property file
     */
    public static String getPropertyFileName(){
        return propertyFileName;
    }

    /**
     * 
     * @return Rectangle stating position of the main window frame. Empty String
     * will be returned if the frame position is not yet set
     */
    public Rectangle getFramePosition() {
        String frameProperty = propertiesSaved.getProperty(PROPERTY_FRAME_POSITION);
        if(frameProperty==null){
            return new Rectangle();
        } else {
            String[] positions = frameProperty.split(",");
            return new Rectangle(Integer.parseInt(positions[0]),
                    Integer.parseInt(positions[1]),
                    Integer.parseInt(positions[2]),
                    Integer.parseInt(positions[3]));   
        }        
    }    
    
    /**
     * Set password for DataPoint to the stored properties and updates properties
     * file on the disk
     */    
    public void setPassword(String cryptedPassword){
        propertiesSaved.setProperty("password", cryptedPassword);
        writePropertiesToFile();
    }

    /**
     * Set username for DataPoint to the stored properties and updates properties
     * file on the disk
     */        
    public void setUserName(String userName){
        propertiesSaved.setProperty("userName", userName);
        writePropertiesToFile();
    }

    /**
     * Set port for DataPoint to the stored properties and updates properties
     * file on the disk
     */      
    public void setPortDataPoint(String portDataPoint) {
        propertiesSaved.setProperty(PROPERTY_PORT, portDataPoint);
        writePropertiesToFile();
    }

    /**
     * Set hostname for DataPoint to the stored properties and updates properties
     * file on the disk
     */      
    public void setHostNameDataPoint(String hostNameDataPoint) {
        propertiesSaved.setProperty(PROPERTY_HOST_NAME,hostNameDataPoint);
        writePropertiesToFile();
    }

    /**
     * Set local path where the downloaded files will be saved to the stored
     * properties and updates properties file on the disk
     */      
    public void setLocalPathRoot(String localPathRoot) {
        propertiesSaved.setProperty(PROPERTY_LOCAL_ROOT_PATH,localPathRoot);
        writePropertiesToFile();
    }

    
    /**
     * Sets File object representing last location of the upload filechooser
     * @param lastUploadDirectory  File object representing last location of the
     * upload filechooser
     */
    public void setLastUploadDirectory(File lastUploadDirectory) {
        propertiesNotSaved.setProperty(PROPERTY_LAST_UPLOAD_DIR, lastUploadDirectory.toString());
    }
    
    /**
     * Sets location of the main frame to the properties
     * @param rectangle Rectangle representing position of the frame
     */
    public void setFramePosition(Rectangle rectangle){
        String property = rectangle.x+","+
                rectangle.y+","+
                rectangle.width+","+
                rectangle.height;
        propertiesSaved.setProperty(PROPERTY_FRAME_POSITION, property);
        writePropertiesToFile();
    }
    
    /**
     * Write properties defined in this object to file on disk
     */
    private void writePropertiesToFile() {
        try {
            File file = new File(propertyFileName);
            FileOutputStream fileOut = new FileOutputStream(file);
            propertiesSaved.store(fileOut, "Connections properties");
            fileOut.close();
        } catch (FileNotFoundException e) {
            log.error("File not accessible when saving property file", e);
        } catch (IOException e) {
            log.error("IO exception when saving property file",e);
        }
    }

    /**
     * Load properties from the saved file
     */
    private void readPropertiesFromFile() {
        try {
            FileInputStream fileInput = new FileInputStream(propertyFileName);
            Properties readProperties = new Properties();
            readProperties.load(fileInput);
            fileInput.close();

            propertiesSaved = readProperties;

        } catch (FileNotFoundException e) {
            log.error("File not found when loading property file", e);
        } catch (IOException e) {
            log.error("IO exception when saving property file",e);
        }
    }
}
