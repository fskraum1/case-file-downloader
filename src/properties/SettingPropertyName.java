/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package properties;

/**
 *
 * @author m.kraus
 */
public enum SettingPropertyName {
    //property names

    PROPERTY_HOST_NAME("propertyHostName"),
    PROPERTY_PORT("propertyPort"),
    PROPERTY_LOCAL_ROOT_PATH("propertyLocalRootPath"),
    PROPERTY_REMOTE_ROOTS_LIST("propertyRemoteRootsList"),
    PROPERTY_LAST_UPLOAD_DIR("propertyLastUploadDir"),
    PROPERTY_LOGIN("propertyLogin"),
    PROPERTY_PASSWORD("propertyPassword"),
    PROPERTY_FRAME_POS_X("propertyFramePosX"),
    PROPERTY_FRAME_POS_Y("propertyFramePosY"),
    PROPERTY_FRAME_WIDTH("propertyFrameWidth"),
    PROPERTY_FRAME_HEIGHT("propertyFrameHeight");
    
    
    private String settingPropertyName;

    SettingPropertyName(String settingPropertyName) {
        this.settingPropertyName = settingPropertyName;
    }

    @Override
    public String toString() {
        return this.settingPropertyName;
    }
}
