/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import javax.swing.JProgressBar;

/**
 * Class containing progress for the Datapoint upload/download. It should contain
 * some area containing progress messages and progress bar
 * @author m.kraus
 */
public interface ProgressContainer {

    /**
     * Append progress to the details textarea
     * @param progressUpdate String to be appended to the textarea
     */
    void appendProgress(String progressUpdate);

    /**
     *
     * @return Return progress bar component which should contain progress about 
     * upload and download
     */
    JProgressBar getProgressBar();
    
}
