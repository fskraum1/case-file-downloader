/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Frame;
import javax.swing.JOptionPane;
import main.CasefileDownloader;
import main.ProgressContainer;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * @author m.kraus
 */
public class ErrorDialogShower {
    private static final ProgressContainer PROGRESS_CONTAINER = CasefileDownloader.getProgressContainer();
    private static final Frame MAIN_FRAME = CasefileDownloader.getFrame();

    private ErrorDialogShower() {        
    }
    
    
    
    /**
     * Displays custom error dialog to user with fixed message "Error during 
     * processing your request, please check the application.log"
     */
    public static void showErrorDialog() {
        JOptionPane.showMessageDialog(MAIN_FRAME, "Error during processing your request, please check the application.log", "Fatal Error", JOptionPane.ERROR_MESSAGE);
    }
    
    
    /**
     * Displays custom error dialog to user and updates localized message and 
     * stack trace of error in the progress area of the CasefileDownloader main
     * frame
     * @param dialogOwner 
     * @param e 
     */
    public static void showErrorDialog(Throwable e) {
        showErrorDialog(e.getLocalizedMessage(), e);
    }
    
    
    public static void showErrorDialog(String customMessage, Throwable e){
        PROGRESS_CONTAINER.appendProgress(ExceptionUtils.getMessage(e));
        PROGRESS_CONTAINER.appendProgress(ExceptionUtils.getStackTrace(e));
        JOptionPane.showMessageDialog(MAIN_FRAME, customMessage, "Fatal Error", JOptionPane.ERROR_MESSAGE);
    }
}
