/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.text.JTextComponent;

/**
 * This action will perform regular paste if the source of the action is descendant 
 * of the JTextComponent and then perform updating of the upload fields
 * @author m.kraus
 */
public class UploadPasteAction extends AbstractAction {

    private PanelUpload panelUpload;

    public UploadPasteAction(PanelUpload panelUpload) {        
        this.panelUpload = panelUpload;
    }

    @Override
    public void actionPerformed(ActionEvent tf) {

        if (tf.getSource() instanceof JTextComponent) {
            JTextComponent textField = (JTextComponent) tf.getSource();
            textField.paste();
            panelUpload.updateUploadFields();
        } else {
        }

    }
}
