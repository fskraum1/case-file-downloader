/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import main.CasefileDownloader;
import main.CurrentVersion;

/**
 *
 * @author m.kraus
 */
public class About {
    
    public static void showAboutDialog(){
        String gplMessage = "Case File Downloader version "+ CurrentVersion.getCurrentVersion()+"\n"
                + "Application is intended to be used by GMC Support department for downloading and uploading "
                + "files from/to DataPoint "
                + "Copyright (C) 2013  Miroslav Kraus \n\n"
                + "This program is free software: you can redistribute it and/or modify "
                +"it under the terms of the GNU General Public License as published by "
                +"the Free Software Foundation, either version 3 of the License, or "
                +"(at your option) any later version.\n\n"
                +"This program is distributed in the hope that it will be useful, "
                +"but WITHOUT ANY WARRANTY; without even the implied warranty of "
                +"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the "
                +"GNU General Public License for more details. \n\n"
                +"You should have received a copy of the GNU General Public License  "
                +"along with this program.  If not, see http://www.gnu.org/licenses/. ";
        
        JTextArea msg = new JTextArea(gplMessage);
        msg.setLineWrap(true);
        msg.setWrapStyleWord(true);
        msg.setRows(10);
        msg.setColumns(50);
        
        JScrollPane scrollPane = new JScrollPane(msg);        
        JOptionPane.showMessageDialog(CasefileDownloader.getFrame(), scrollPane, "About", JOptionPane.INFORMATION_MESSAGE);
    }
}
