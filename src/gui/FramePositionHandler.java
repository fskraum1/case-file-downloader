/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Rectangle;
import javax.swing.JFrame;
import properties.SettingsProperties;

/**
 * Used to set initial location of the frame
 * @author m.kraus
 */
public class FramePositionHandler {
    private JFrame frame;

    /**
     * Creates new FramePositionHandler
     * @param frame Frame for which the position will be set
     */
    public FramePositionHandler(JFrame frame) {
        this.frame = frame;
    }
    
    /**
     * Store current frame position to SettingsProperties for later retrieval
     */
    public void storePosition(){
        Rectangle r = frame.getBounds();
        SettingsProperties.getInstance().setFramePosition(r);                
    }
    
    /**
     * Sets the frame location based on the previously stored value in the 
     * SettingsProperties. In case the SettingProperties does not contain location
     * yet, position of frame is set by platform.
     * @see JFrame#setLocationByPlatform(boolean) 
     */
    public void restorePosition(){
        Rectangle r = SettingsProperties.getInstance().getFramePosition();
        if(r.isEmpty()){
            frame.setLocationByPlatform(true);
        } else {
            frame.setLocationByPlatform(false);
            frame.setBounds(r);
        }
    }
}
