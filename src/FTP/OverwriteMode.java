/*
 * Application is intended to be used by GMC Support department for downloading 
 * files from DataPoint
 * Copyright (C) 2013  Miroslav Kraus

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License    
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package FTP;

/**
 *
 * @author m.kraus
 */
public enum OverwriteMode {
    /**
     * All files with the same name as in target should be overwritten
     */
    OVERWRITE_ALL,
    
    /**
     * All files with the same name as in target should be skipped
     */
    SKIP_ALL,
    
    /**
     * All files in target folder which are older then the copied should be overwritten
     */
    OVERWRITE_OLDER,
    
    /**
     * Target file with the same name as the copied one should be renamed
     */
    RENAME_TARGET,
    
    /**
     * File with the same name as in target should be renamed automatically
     */
    RENAME_COPIED,
    
    /**
     * All target files with the same name as the copied one should be renamed automatically
     */
    AUTO_RENAME_TARGET,
    
    /**
     * All files with the same name as in target should be renamed automatically
     */
    AUTO_RENAME_COPIED,
    
    /**
     * Currently copied file will be overwritten
     */
    OVERWRITE_THIS,
    
    /**
     * Currently copied file will be skipped
     */
    SKIP_THIS,
    
    /**
     * None mode
     */
    NONE,
    ;
    
}
