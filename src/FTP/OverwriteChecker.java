/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FTP;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import gui.DialogOverwrite;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;
import main.CasefileDownloader;
import static FTP.OverwriteMode.*;
import org.apache.log4j.Logger;

/**
 *
 * @author m.kraus
 */
public class OverwriteChecker {
    static Logger log = Logger.getLogger(OverwriteChecker.class);
    
    private OverwriteMode currentOverwriteMode;

    public OverwriteChecker(OverwriteMode currentOverwriteMode) {
        this.currentOverwriteMode = currentOverwriteMode;
        
    }
        
    

    /**
     * Returns if the current remote file can be overwritten. In case the file
     * with the same name is found as the one which should be uploaded it will
     * initiate dialog to find out user overwrite mode and return true or false
     * based on the specified mode.
     * 
     * @param source    Source file to upload
     * @param destination   Destination on sFTP
     * @param channel       Channel to the sFTP
     * @return  <code>true</code> in case file should be overwritten, <code>false</code> if not
     * @throws SftpException 
     */

    public boolean checkOverwriteUploading(String source, String destination, ChannelSftp channel) {
        Vector<ChannelSftp.LsEntry> destinations;
        try {
            destinations = channel.ls(destination);
        } 
        catch (SftpException ex) {
            return true;    //no file has been found, upload can be initiated returning true;
        }
        
        String remoteDirectory = destination.substring(0, destination.lastIndexOf("/"));
        return checkOverwrite(destinations.firstElement(), remoteDirectory, source, Boolean.TRUE);
    }
    
    /**
     * 
     * Returns if the current remote file can be overwritten. In case the file
     * with the same name is found as the one which should be downloaded it will
     * initiate dialog to find out user overwrite mode and return true or false
     * based on the specified mode.
     *
     * @param currentRemoteFile sFTP remote file which should be downloaded
     * @param localFilePath path to the local file where the sFTP file should
     * @param remoteDirectory remote directory path of the currently downloaded
     * currentRemoteFile be downloaded
     * @return <code>true</code> if the local file should be overwritten with
     * the remote file; <code>false</code> otherwise
     */
    public boolean checkOverwriteDownloading(ChannelSftp.LsEntry currentRemoteFile, String remoteDirectory, String localFilePath){
        File localFile = new File(localFilePath);
        if(localFile.exists()){
            return checkOverwrite(currentRemoteFile, remoteDirectory, localFilePath, Boolean.FALSE);            
        }
        else{
            return true;            
        }
    }
    
    /**
     * Returns current overwrite mode. Current overwrite mode is changed after 
     * performing either checkOverwriteDownloading or checkOverwriteUploading 
     * call
     * @return current overwrite mode
     */
    public OverwriteMode getCurrentOverwriteMode(){
        return currentOverwriteMode;
    }

    /**
     * Returns if the current remote file can be overwritten. In case the file
     * with the same name is found as the one which should be downloaded it will
     * initiate dialog to find out user overwrite mode and return true or false
     * based on the specified mode.
     *
     * @param currentRemoteFile sftp remote file which should be downloaded
     * @param localFilePath path to the local file where the sFTP file should
     * @param remoteDirectory remote directory path of the currently downloaded
     * currentRemoteFile be downloaded
     * @return <code>true</code> if the local file should be overwritten with
     * the remote file; <code>false</code> otherwise
     */
    private boolean checkOverwrite(ChannelSftp.LsEntry currentRemoteFile, String remoteDirectory, String localFilePath, Boolean uploading) {
        File localFile = new File(localFilePath);
//this tests if overwrite mode has not been set to some automatic values like overwrite all or autorename copied - means we need to display dialog 
        if (currentOverwriteMode == OverwriteMode.NONE || currentOverwriteMode == OverwriteMode.OVERWRITE_THIS      
                || currentOverwriteMode == OverwriteMode.SKIP_THIS || currentOverwriteMode == OverwriteMode.RENAME_COPIED
                || currentOverwriteMode == OverwriteMode.RENAME_TARGET) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
            long sizeOfLocal = localFile.length();
            Date lastModifiedLocal = new Date(localFile.lastModified());

            SftpATTRS currentRemoteAtrs = currentRemoteFile.getAttrs();
            long sizeOfRemote = Long.valueOf(currentRemoteAtrs.getSize());
            log.debug(String.valueOf(Long.valueOf(currentRemoteAtrs.getMTime()) * 1000));
            Date lastModifiedRemote = new Date(Long.valueOf(currentRemoteAtrs.getMTime()) * 1000);

            DialogOverwrite dialogOverwrite = new DialogOverwrite(CasefileDownloader.getFrame(), true);
            String localNameLabel = "<html><p>"+localFilePath+"</html></p>";
            String localDetailsLabel = String.valueOf(sizeOfLocal) + " bytes, " + simpleDateFormat.format(lastModifiedLocal);
            String remoteNameLabel = "<html><p>"+remoteDirectory + "/" + currentRemoteFile.getFilename()+"</html></p>";
            String remoteDetailsLabel = String.valueOf(sizeOfRemote) + " bytes, " + simpleDateFormat.format(lastModifiedRemote);
            
            if(uploading){
                dialogOverwrite.setLabelSourceName(localNameLabel);
                dialogOverwrite.setLabelSourceFileDetails(localDetailsLabel);                
            
                dialogOverwrite.setLabelTargetName(remoteNameLabel);
                dialogOverwrite.setLabelTargetFileDetails(remoteDetailsLabel);
                
                //Disabling buttons which make no sense for upload
                dialogOverwrite.setEnabledButtonsForPersistentModes(false);
            }
            else{
                dialogOverwrite.setLabelSourceName(remoteNameLabel);
                dialogOverwrite.setLabelSourceFileDetails(remoteDetailsLabel);
                
                dialogOverwrite.setLabelTargetName(localNameLabel);
                dialogOverwrite.setLabelTargetFileDetails(localDetailsLabel);                                            
            }
            
            dialogOverwrite.setLocationRelativeTo(CasefileDownloader.getFrame());
            dialogOverwrite.setVisible(true);            
            currentOverwriteMode = dialogOverwrite.getCurrentOverwriteMode();
        }

        switch (currentOverwriteMode) {
            case SKIP_ALL:
                return false;
            case OVERWRITE_ALL:
                return true;
            case OVERWRITE_THIS:
                return true;
            case SKIP_THIS:
                return false;
            case AUTO_RENAME_COPIED:
                return true;
            case AUTO_RENAME_TARGET:
                return true;
            case RENAME_COPIED:
                return true;
            case RENAME_TARGET:
                return true;
            case OVERWRITE_OLDER:
                Date lastModifiedLocal = new Date(localFile.lastModified());
                SftpATTRS currentRemoteAtrs = currentRemoteFile.getAttrs();
                Date lastModifiedRemote = new Date(Long.valueOf(currentRemoteAtrs.getMTime()) * 1000);  //TODO> same lines as above this variable should be created only on one place since it has same content
                if (lastModifiedLocal.compareTo(lastModifiedRemote) > 0) {
                    return false;
                } else {
                    return true;
                }
            default:
                throw new Error("Unexpected overwrite mode " + currentOverwriteMode.toString());
            //log.error("Unexpected overwrite mode "+currentOverwriteMode.toString(),);                        
        }
    }
}
