/*
 * Application is intended to be used by GMC Support department for downloading 
 * files from DataPoint
 * Copyright (C) 2013  Miroslav Kraus

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License    
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package FTP;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import gui.ErrorDialogShower;
import gui.PanelUpload;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import javax.swing.SwingWorker;
import main.CasefileDownloader;
import main.ProgressContainer;
import properties.SettingsProperties;
import org.apache.log4j.Logger;

/**
 *
 * @author m.kraus
 */
public class UploadFieldsUpdater extends SwingWorker<String, String>{
    private static final ProgressContainer PROGRESS_CONTAINER = CasefileDownloader.getProgressContainer();
    private static final SettingsProperties SETTINGS_PROPERTIES = SettingsProperties.getInstance();
    private static final Logger log = Logger.getLogger(UploadFieldsUpdater.class);

    private ChannelSftp channel;
    private String targetTicketDirectory;
    private String targetFileName;
    private String ticketId;
    private PanelUpload panelUpload;
    
    /**
     * Create UploadFieldsUpdater object based on the File pasted in parameter
     * @param sourceFile Pointer to file to be uploaded on local drive
     * @throws JSchException
     * @throws GeneralSecurityException
     * @throws IOException 
     */

    public UploadFieldsUpdater(File sourceFile, PanelUpload panelUpload) throws JSchException, GeneralSecurityException, IOException {
        String sourceFileAbsolutePath = firstCharToLowercase(sourceFile.getAbsolutePath());
        String localPathRoot = firstCharToLowercase(SETTINGS_PROPERTIES.getLocalPathRoot());
        if(sourceFileAbsolutePath.contains(localPathRoot)){     //if sourcefile is on the default local path (something like N:\support\..\HF)
            String subStringWithoutLocalPath = sourceFileAbsolutePath.substring(localPathRoot.length(), sourceFileAbsolutePath.length());   //
            this.ticketId=subStringWithoutLocalPath.split("\\\\")[0];   //top directory
            this.targetTicketDirectory = subStringWithoutLocalPath.substring(0, subStringWithoutLocalPath.lastIndexOf("\\")).replace("\\", "/");   //entire directory without filename
        }
        else{
            this.ticketId="";
            this.targetTicketDirectory="";
        }        
        this.targetFileName = sourceFile.getName();
                
        DpSftpChannel dpSftpChannel = new DpSftpChannel();
        this.channel = dpSftpChannel.getSftpChannel();
        this.panelUpload = panelUpload;
    }       
    
    
    /**
     * 
     * @return target directory in the region root
     */
    public String getTargetTicketDirectory(){
        return targetTicketDirectory;
    }
    
    /**
     * 
     * @return filename of the target directory
     */
    public String getTargetFileName(){
        return targetFileName;
    }

    @Override
    protected String doInBackground() {
        panelUpload.getProgressBar().setIndeterminate(true);
        publish("Trying to find corresponding directory on Data point \n");
        String[] dpRootsList = SETTINGS_PROPERTIES.getDataPointRootsList().split("\\|");
        if(!ticketId.isEmpty()) {
            for (String currentDpRoot : dpRootsList) {
                try {
                    channel.cd(currentDpRoot + "/" + ticketId);
                    publish("Directory found on DP - " + currentDpRoot + "/" + ticketId + "\n");
                    return currentDpRoot;
                } catch (SftpException ex) {
                    continue;
                }
            }
        }
        publish("Directory was not found on DP\n");
        return "";  //in case the directory is not foudn empty string is returned
    }
    
     @Override
     protected void process(List<String> chunks) {
         for (String progress : chunks) {
             log.info(progress);
             PROGRESS_CONTAINER.appendProgress(progress);
         }
     }
     
    @Override
    protected void done() {
        panelUpload.getProgressBar().setIndeterminate(false);
        try {
            panelUpload.setComboTarget(get());
        } catch (InterruptedException | ExecutionException ex) {
            ErrorDialogShower.showErrorDialog(ex);
            log.error(ex.getMessage(), ex);            
        }
    }
    
    /**
     * Make the first character of the input string lower case and return the string
     * @param inputString Input string which first letter should be lowercased
     * @return String with the first character changed to the lowercase
     */
    private String firstCharToLowercase(String inputString){
        inputString = Character.toLowerCase(
            inputString.charAt(0)) + (inputString.length() > 1 ? inputString.substring(1) : "");
        return inputString;
    }
}
