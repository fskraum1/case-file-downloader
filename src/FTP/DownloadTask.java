/*
 * Application is intended to be used by GMC Support department for downloading 
 * files from DataPoint
 * Copyright (C) 2013  Miroslav Kraus

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License    
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package FTP;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Vector;
import javax.swing.SwingWorker;
import utils.FileUtil;
import properties.SettingsProperties;
import gui.ErrorDialogShower;
import gui.PanelDownload;
import main.CasefileDownloader;
import main.ProgressContainer;
import org.apache.log4j.Logger;

/**
 * DownloadTask is extending SwingWorker<String, String> and its main purpose is
 * to download files from SFTP 
 * 
 * This doInBackground() method is returning string 
 * @author m.kraus
 */
public class DownloadTask extends SwingWorker<String, String> {
    private SettingsProperties settingsProperties = SettingsProperties.getInstance();
    private static Logger log = Logger.getLogger(DownloadTask.class.getName());
    private ProgressContainer progressContainer = CasefileDownloader.getProgressContainer();
    private int downloadedFilesCount;
    private OverwriteMode currentOverwriteMode;
    private ChannelSftp sftpChannel;
    private String ticketId;
    private PanelDownload panelDownload;

    /**
     * Creates new download task
     * @param ticketId Ticket ID for which the files should be downloaded
     */
    public DownloadTask(String ticketId, PanelDownload panelDownload) {
        this.currentOverwriteMode = OverwriteMode.NONE;
        this.ticketId = ticketId;
        this.panelDownload = panelDownload;
    }
    
    

    /**
     * Main task. Executed in background thread.
     * @return returns directory to which the files has been downloaded
     */
    @Override
    public String doInBackground() {
        publish("Initializing sftp connection to DataPoint \n");
        if (ticketId.isEmpty()) { //download button should be always disabled if empty string is in ticket ID but just in case :)            
            String message = "Filled ticket ID is empty, please fill ticket ID correctly";
            log.error(message);
            throw new IllegalArgumentException(message);
        }
        
        String localDirectory ="";
        try {
            DpSftpChannel dpSftpChannel = new DpSftpChannel();
            sftpChannel = dpSftpChannel.getSftpChannel();
            String remoteTicketDirectory = getRemoteTicketDirectory(ticketId);
            String message = "Directory on DataPoint found: " + remoteTicketDirectory + "\n";
            publish(message);
            log.info(message);
            localDirectory = settingsProperties.getLocalPathRoot() + ticketId;
            getDirectoryFromSftp(remoteTicketDirectory, localDirectory);
            dpSftpChannel.closeChannel();
            log.info("sFTP session disconnected");
        } catch (JSchException e) {
            String message = "Exception when connecting to the sFTP";
            ErrorDialogShower.showErrorDialog(message, e);
            log.error(message, e);
        } catch (GeneralSecurityException e) {
            String message = "General Security exception when connecting to the sFTP";
            ErrorDialogShower.showErrorDialog(message, e);
            log.error(message, e);
        } catch (IOException e) {
            String message = "Input output exception raised when connecting to the sFTP";
            ErrorDialogShower.showErrorDialog(message, e);
            log.error(message, e);
        } catch (Exception e) {
            String message = "Remote directory not found";
            ErrorDialogShower.showErrorDialog(message, e);
            log.error(message, e);
        }
        return localDirectory;
    }

    /*
     * Executed in event dispatch thread
     */
    @Override
    public void done() {
        panelDownload.downloadFinished(downloadedFilesCount); 
        
    }

    @Override
    protected void process(List<String> chunks) {
        // Updates the messages text area
        for (String string : chunks) {
            progressContainer.appendProgress(string);
        }
    }

    /**
     * Tries to find the Ticket directory on the sFTP. In case the directory is
     * found its path is returned else exception is raised
     *
     * @param sftpChannel ChannelSftp object containing open connection to the
     * sFTP
     * @param ticketId ID of the case
     * @return Path to the directory of the ticket passed as parameter
     */
    private String getRemoteTicketDirectory(String ticketId) throws Exception {
        String remoteTicketDirectory = null;
        boolean remoteTicketDirectoryFound = false;        
        String[] dataPointRootsList=settingsProperties.getDataPointRootsList().split("\\|");        
        for (String currentDataPointRoot : dataPointRootsList) {
            try {
                sftpChannel.cd(currentDataPointRoot + "/" + ticketId);
                remoteTicketDirectory = currentDataPointRoot + "/" + ticketId;
                remoteTicketDirectoryFound = true;
                continue;
            } catch (SftpException ex) {
                log.info("directory " + currentDataPointRoot + "/" + ticketId + " not found, skipping.",ex);
            }
        }
        if (remoteTicketDirectoryFound) {
            return remoteTicketDirectory;
        } else {
            throw new Exception("Directory with ID " + ticketId + " not found on DP");
        }
    }

    /**
     * Downloads recursively directory from the opened sFTP channel to the local
     * directory
     *
     * @param sftpChannel ChannelSftp object containing open connection to the
     * sFTP
     * @param directoryToDownload Directory which should be downloaded from sFTP
     * @param localDirectory Directory to which the data will be downloaded
     */
    private void getDirectoryFromSftp(String directoryToDownload, String localDirectory) {
        FileUtil.createDirectories(localDirectory);
        try {
            Vector<ChannelSftp.LsEntry> filesInDirectory = sftpChannel.ls(directoryToDownload);
            sftpChannel.cd(directoryToDownload);
            OverwriteChecker o = new OverwriteChecker(currentOverwriteMode);  //TODO> maybe create object in upper call so the o is not created for every directory recursivelly           
            for (ChannelSftp.LsEntry currentRemoteFile : filesInDirectory) { // iterate through objects in list, identifying specific file names
                sftpChannel.cd(directoryToDownload);
                if (!currentRemoteFile.getAttrs().isDir()) {    
                    getFileFromSftp(currentRemoteFile, o, directoryToDownload, localDirectory);
                } else {
                    progressContainer.appendProgress("Subdirectory found - '" + currentRemoteFile.getFilename() + "'\n");
                    getDirectoryFromSftp(directoryToDownload + "/" + currentRemoteFile.getFilename(), localDirectory + "\\" + currentRemoteFile.getFilename());
                    log.debug("directory to call Iterate " + currentRemoteFile.getFilename());
                }

            }
        } catch (SftpException ex) {
            String message = "General error during downloading directory " + directoryToDownload + " to " + localDirectory + " with channel " + sftpChannel;
            ErrorDialogShower.showErrorDialog(message, ex);
            log.error(message);
        }
    }

    private void getFileFromSftp(ChannelSftp.LsEntry currentRemoteFile, OverwriteChecker o, String directoryToDownload, String localDirectory) throws SftpException {
        String remoteFileName = currentRemoteFile.getFilename();
        DatapointSftpProgressMonitor progressMonitor = new DatapointSftpProgressMonitor(progressContainer.getProgressBar());
        progressContainer.appendProgress("Downloading file " + remoteFileName + "...");
        Boolean overwrite = o.checkOverwriteDownloading(currentRemoteFile, directoryToDownload, localDirectory + "\\" + remoteFileName);
        currentOverwriteMode = o.getCurrentOverwriteMode();        
        if (overwrite && (currentOverwriteMode == OverwriteMode.AUTO_RENAME_COPIED || currentOverwriteMode == OverwriteMode.RENAME_COPIED)) {
            String newFileName = FileUtil.getNewName(remoteFileName);
            sftpChannel.get(remoteFileName, localDirectory + "\\" + newFileName, progressMonitor);
            downloadedFilesCount += 1;
            publish(" done\n");
            publish("Copied file renamed to " + newFileName + "\n");
        } else if (overwrite && (currentOverwriteMode == OverwriteMode.AUTO_RENAME_TARGET || currentOverwriteMode == OverwriteMode.RENAME_TARGET)) {
            String newFileName = FileUtil.getNewName(remoteFileName);
            File localFile = new File(localDirectory + "\\" + remoteFileName);
            localFile.renameTo(new File(localDirectory + "\\" + newFileName));
            sftpChannel.get(remoteFileName, localDirectory + "\\" + remoteFileName, progressMonitor);
            downloadedFilesCount += 1;
            publish(" done\n");
            publish("Target file renamed to " + newFileName + "\n");
        } else if (overwrite) {
            sftpChannel.get(remoteFileName, localDirectory + "\\" + remoteFileName, progressMonitor);
            downloadedFilesCount += 1;
            publish(" done\n");
        } else {
            publish(" file skipped\n");
        }
    }    
}
