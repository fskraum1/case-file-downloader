/*
 * Application is intended to be used by GMC Support department for downloading 
 * files from DataPoint
 * Copyright (C) 2013  Miroslav Kraus

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License    
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package FTP;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import java.io.IOException;
import java.security.GeneralSecurityException;
import main.Password;
import properties.SettingsProperties;
import org.apache.log4j.Logger;

/**
 * DataPoint SFTP channel
 * @author m.kraus
 */
public class DpSftpChannel {

    private SettingsProperties settingsProperties = SettingsProperties.getInstance();
    static Logger log = Logger.getLogger(DownloadTask.class.getName());
    private final ChannelSftp sftpChannel;
    private Session session;
    
    /**
     * Creates new SFTP channel pointing to Datapoint based on values saved in the 
     * SettingsProperties
     * @throws JSchException
     * @throws GeneralSecurityException
     * @throws IOException 
     */
    public DpSftpChannel() throws JSchException, GeneralSecurityException, IOException {
        log.debug("Initializing sftp connection to DataPoint \n");
        JSch jsch = new JSch();
        session = jsch.getSession(settingsProperties.getUsername(), settingsProperties.getHostNameDataPoint(), settingsProperties.getPortDataPoint());
        session.setConfig("StrictHostKeyChecking", "no");
        session.setPassword(new Password().decrypt(settingsProperties.getPassword()));
        log.debug("session" + session);
        session.connect();
        log.info("Successfully connected to Datapoint \n");

        Channel channel = session.openChannel("sftp");
        channel.connect();
        
        sftpChannel = (ChannelSftp) channel;
    }
    
    /**
     * Returns opened sftp channel to datapoint, if the channel is already closed
     * it will open it once again
     * @return opened sftpChannel
     */
    public ChannelSftp getSftpChannel() throws JSchException{
        if(sftpChannel.isConnected()){
            return sftpChannel;
        }
        else{
            session.connect();
            sftpChannel.connect();
        }
        return sftpChannel;
    }
    
    
    /**
     * Closes opened channel
     */
    public void closeChannel(){
        sftpChannel.disconnect();
        session.disconnect();
        
    }
}
