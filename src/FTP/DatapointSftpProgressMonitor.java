/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FTP;

import com.jcraft.jsch.SftpProgressMonitor;
import javax.swing.JProgressBar;

/**
 *
 * @author m.kraus
 */
public class DatapointSftpProgressMonitor implements SftpProgressMonitor {
    JProgressBar progressBar;
    long count = 0;
    long max = 0;
    private long percent = -1;
    private Boolean originaIndeterminateMode;
    
    public DatapointSftpProgressMonitor(JProgressBar progressBar) {
        this.progressBar = progressBar;
    }        

    @Override
    public void init(int op, String src, String dest, long max) {
        this.max = max;
        originaIndeterminateMode = progressBar.isIndeterminate();
        progressBar.setIndeterminate(false);
        progressBar.setMinimum(0);
        progressBar.setMaximum((int)max);
        progressBar.setString(((op == SftpProgressMonitor.PUT) ? "put" : "get") + ": " + src);
        progressBar.setStringPainted(true);

        count = 0;
        percent = -1;      
    }
    

    @Override
    public boolean count(long count) {
        this.count += count;

        if (percent >= this.count * 100 / max) {
            return true;
        }
        percent = this.count * 100 / max;

        progressBar.setString("Completed " + humanReadableByteCount(this.count) + "(" + percent + "%) out of " + humanReadableByteCount(max) + ".");
        progressBar.setValue((int) this.count);

        return true; //!(progressBar.isCanceled());
    }

    @Override
    public void end() {
        progressBar.setIndeterminate(originaIndeterminateMode);
    }

    /**
     * Convert byte size into human-readable format - Like 1024 
     * become "1 KB" and 1024*1024 should become "1 MB".
     * I am kind of sick of writing this utility method for each project. Are
     * there any static methods in Apache Commons for this?
     *
     * @param bytes Number to transform
     * @return
     */
    private String humanReadableByteCount(long bytes) {
        int unit = 1024;
        if (bytes < unit) {
            return bytes + " B";
        }
        int exp = (int) (Math.log(bytes) / Math.log(unit));
        String prefixes = "KMGTPE";
        String prefix = String.valueOf(prefixes.charAt(exp - 1));
        return String.format("%.1f %sB", bytes / Math.pow(unit, exp), prefix);
    }
}
