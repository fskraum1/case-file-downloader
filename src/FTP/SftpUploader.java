/*
 * Application is intended to be used by GMC Support department for downloading 
 * files from DataPoint
 * Copyright (C) 2013  Miroslav Kraus

 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the   
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License    
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package FTP;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import gui.ErrorDialogShower;
import java.awt.HeadlessException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;
import main.CasefileDownloader;
import main.ProgressContainer;
import utils.FileUtil;
import org.apache.log4j.Logger;

/**
 *
 * @author m.kraus
 */
public class SftpUploader extends SwingWorker<Void, String>{
    private static final ProgressContainer PROGRESS_CONTAINER = CasefileDownloader.getProgressContainer();
    private static final Logger log = Logger.getLogger(SftpUploader.class);
    private OverwriteMode currentOverwriteMode=OverwriteMode.NONE;

    private ChannelSftp channel;
    private String sourceFile;
    private String destFile;
    
    /**
     * Creates New SftpUploader which extends SwingWorker
     * @param sourceFile String containing to the local path to the file to be uploaded
     * @param destFile String containing to the path to the file where the file should be uploaded
     * @throws JSchException
     * @throws GeneralSecurityException
     * @throws IOException 
     */
    public SftpUploader(String sourceFile, String destFile) throws JSchException, GeneralSecurityException, IOException {
        DpSftpChannel dpSftpChannel = new DpSftpChannel();        
        this.channel = dpSftpChannel.getSftpChannel();
        this.sourceFile = sourceFile;
        this.destFile = destFile;
        PROGRESS_CONTAINER.appendProgress("connected to sFTP \n");
    }        
    
    /**
     * This method should be called by the execute method from super. It should 
     * never be called directly
     * @return null
     * @throws NoSuchMethodException 
     */
    @Override
    protected Void doInBackground() throws NoSuchMethodException {
        PROGRESS_CONTAINER.getProgressBar().setIndeterminate(true);
        publish("Put to sFTP initiated\n");
        OverwriteChecker overwrite = new OverwriteChecker(currentOverwriteMode);
        publish("Checking existence of the target file " + destFile + "\n");
        if (overwrite.checkOverwriteUploading(sourceFile, destFile, channel)) {
            defineOverwrite(overwrite);
            publish("Checking if the dest directory exists ...");
            String destFileParent = destFile.substring(0, destFile.lastIndexOf("/"));
            try {
                channel.cd(destFileParent);
                publish("found\n");
                putToSftp();
            } catch (SftpException e) {
                Boolean createDirectory = verifyDirectoryCreation(destFileParent);
                if (createDirectory) {
                    createRemoteFolder(destFileParent);
                    publish("created\n");
                    putToSftp();
                } else {
                    publish("skipped\n");
                }
            }
        }
        return null;
    }
    

    @Override
    protected void done() {
            PROGRESS_CONTAINER.getProgressBar().setIndeterminate(false);
            publish("**** Upload finished ****\n");
    } 
    
    
    @Override
    protected void process(List<String> chunks) {
        for (final String currentChunk : chunks) {
            PROGRESS_CONTAINER.appendProgress(currentChunk);
            log.info(currentChunk);
        }
    }
    
    private void putToSftp() {
        publish("Uploading " + sourceFile + " ...");
        try {
            channel.put(sourceFile, destFile, new DatapointSftpProgressMonitor(PROGRESS_CONTAINER.getProgressBar()));
        } catch (SftpException e) {
            String message = "Input output exception raised when uploading file to sFTP";
            ErrorDialogShower.showErrorDialog(message, e);
            log.error(message, e);
        }
        publish(" Done\n");
    }

    /**
     * if target directory does not exist dialog is raised and user response is returned
     * @param destFileParent Remote directory which should be verified to create
     * @return Returns true in case the directory should be created, false if
     * the directory should not be created
     * @throws HeadlessException 
     */
    private Boolean verifyDirectoryCreation(String destFileParent) throws HeadlessException {
        int n = JOptionPane.showConfirmDialog(
                CasefileDownloader.getFrame(), //parrent window
                "Directory " + destFileParent + " Does not exists. Do you want to create it?", //question in dialog
                "Derectory Creation", //dialog window name
                JOptionPane.YES_NO_OPTION);

        return n == JOptionPane.YES_OPTION;

    }

    /**
     * Creates remote folder on datapoint SFTP
     * @param destFileParent Remote folder to be created
     * @throws NoSuchMethodException 
     */
    private void createRemoteFolder(String destFileParent) throws NoSuchMethodException {
        String[] folders = destFileParent.split("/");
        for (String folder : folders) {
            if (folder.length() > 0) {
                try {
                    channel.cd(folder);
                } catch (SftpException ex) {
                    try{
                        channel.mkdir(folder);
                        channel.cd(folder);
                    }
                    catch(SftpException neverEx){
                        throw new NoSuchMethodException("This exception should never occur, please contact developer of application");
                    }
                }
            }
        }
    }

    /**
     * Based on the passed OverwriteChecker destFile name is defined as well as 
     * any renaming of existing file is performed
     * @param overwrite 
     */
    private void defineOverwrite(OverwriteChecker overwrite) {
        currentOverwriteMode = overwrite.getCurrentOverwriteMode();
        if(currentOverwriteMode.equals(OverwriteMode.RENAME_COPIED)){
            destFile = FileUtil.getNewName(destFile);
            publish("Copied file will be renamed to "+destFile+"\n");
        }
        else if (currentOverwriteMode.equals(OverwriteMode.RENAME_TARGET)){
            String newName = FileUtil.getNewName(destFile);
            publish("Target file will be renamed to "+newName+"\n");
            try {
                channel.rename(destFile, newName);
            }
            catch(SftpException e){
                String message = "Input output exception raised when renaming file on sFTP";
                ErrorDialogShower.showErrorDialog(message, e);
                log.error(message, e);
            }
        }
    }
}
