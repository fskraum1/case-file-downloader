/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import javax.swing.JTextField;

/**
 * Util class for all swing components methods
 * @author m.kraus
 */
public class SwingUtil {

    private SwingUtil() {
    }
    
    /**
     * Select all text in the component which is source of the event.
     * @param evt Event raising the selection of all text. This event has to 
     * originate in the JTextField class
     */
    public static void selectAllTextArea(java.awt.event.FocusEvent evt){
        javax.swing.JTextField newText = (JTextField)evt.getComponent();
        newText.selectAll();
    }
    
    /**
     * Add string to the system clipboard so it is accessible by other
     * application
     * @param clipBoardContent Old clipboard content is discarded and content
     * of this parameter is set to the clipboard
     */
    public static void fillClipboard(String clipBoardContent) {
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        StringSelection stringSelection = new StringSelection(clipBoardContent);
        clipboard.setContents(stringSelection, null);   //not setting clipboard owner since that is not use anywhere else.
    }
}
