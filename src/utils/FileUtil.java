/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author m.kraus
 */
public class FileUtil {
    
    /**
     * For a utility class, which does not require instantiation and only has 
     * static methods, use a private constructor. The compiler can then enforce 
     * that no instances are created. 
     */
    private FileUtil(){
        
    }
    
    /**
     * Creates directory on the local path in case the directory does not exists
     *
     * @param directoryToCreate directory which should be created
     */
    static public void createDirectories(String directoryToCreate) {
        File fDirectoryToCreate = new File(directoryToCreate);
        if (!fDirectoryToCreate.exists()) {
            fDirectoryToCreate.mkdirs();
        }
    }
    
/**
     * Enhance the original name parsed as parameter with string '-Copy' just
     * before extension. If the file does not have extension string '-Copy' is
     * placed to the end of the name
     *
     * @param originalName original name to be changed
     * @return original name enhanced with the string '-Copy' just before
     * extension e.g. fi.le.ext -> fi.le-Copy.ext
     */
    static public String getNewName(String originalName) {
        if (originalName.contains(".")) {
            String newFileName = originalName.substring(0, originalName.lastIndexOf("."));
            newFileName += "-Copy";
            newFileName += originalName.substring(originalName.lastIndexOf("."), originalName.length());
            return newFileName;
        } else {
            return originalName + "-Copy";
        }
    }
    
    /**
     * Deletes directory specified in the parameter recursively
     * @param file File object pointing to the directory to be deleted
     * @throws IOException 
     */
    static public void deleteRecursive(File file) throws IOException {
        if (file.isDirectory()) {
            //directory is empty, then delete it
            if (file.list().length == 0) {
                file.delete();
                //System.out.println("Directory is deleted : " + file.getAbsolutePath());
            } 
            else {
                //list all the directory contents
                String files[] = file.list();
                for (String temp : files) {
                    //construct the file structure
                    File fileDelete = new File(file, temp);
                    //recursive delete
                    deleteRecursive(fileDelete);
                }
                //check the directory again, if empty then delete it
                if (file.list().length == 0) {
                    file.delete();
                    //System.out.println("Directory is deleted : " + file.getAbsolutePath());
                }
            }
        } 
        else {
            //if file, then delete it
            file.delete();
            //System.out.println("File is deleted : " + file.getAbsolutePath());
        }
    }    
}
