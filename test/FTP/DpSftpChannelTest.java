/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FTP;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author m.kraus
 */
public class DpSftpChannelTest {
    private DpSftpChannel instance; 
    
    public DpSftpChannelTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        try {
            instance = new DpSftpChannel();
        } catch (JSchException | GeneralSecurityException | IOException ex) {
            ex.printStackTrace();
            fail("exception in creation of the channel in the setup of the test " + ex);
            System.out.println(ex.getMessage());
        }
    }
    
    @After
    public void tearDown() {
        if(instance!=null){
            instance.closeChannel();
        }        
    }

    /**
     * Test of getSftpChannel method, of class DpSftpChannel.
     */
    @Test
    public void testGetSftpChannel() {
        System.out.println("*** getSftpChannel ***");        
        ChannelSftp result = null;
        try {
            result = instance.getSftpChannel();
        } catch (JSchException ex) {
            fail("error in getting SFTP channel "+ex);
            Logger.getLogger(DpSftpChannelTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        assertTrue("Sftp channel is not opened",result.isConnected());
        System.out.println("*** getSftpChannel success ***");        
    }

    /**
     * Test of closeChannel method, of class DpSftpChannel.
     */
    @Test
    public void testCloseChannel() {
        System.out.println("*** closeChannel ***");
        ChannelSftp result = null;
        try {
            result = instance.getSftpChannel();
        } catch (JSchException ex) {
            fail("error in getting SFTP channel "+ex);
            Logger.getLogger(DpSftpChannelTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        instance.closeChannel();
        assertFalse("channel is not closed", result.isConnected());
        System.out.println("*** closeChannel success ***");        
    }
}