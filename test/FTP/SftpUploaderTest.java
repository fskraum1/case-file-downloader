/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FTP;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import java.io.IOException;
import java.security.GeneralSecurityException;
import properties.SettingsProperties;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author m.kraus
 */
public class SftpUploaderTest {
    private SftpUploader instance;
    private final String remoteTestFolderPath="/Customers All/EMEA/!SUPPORT/test/";
    private String propertyFileName=SettingsProperties.getPropertyFileName();
    
    public SftpUploaderTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
        ChannelSftp channel = openChannel();        
        try {
            channel.rm(remoteTestFolderPath+propertyFileName);
            System.out.println("File on the target directory found, deleting");
        } catch (SftpException ex) {
            System.out.println("File on the target directory not found, deleting skipped");
        }
                
        try {
            instance = new SftpUploader(propertyFileName, remoteTestFolderPath+propertyFileName);
        } catch (JSchException | GeneralSecurityException | IOException ex) {
            fail(ex.getLocalizedMessage());
        }
        channel.disconnect();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of doInBackground method, of class SftpUploader.
     */
    @Test
    public void testExecute() throws Exception {
        System.out.println("*** testExecute ***");                
        instance.execute();        
        
        ChannelSftp channel = openChannel();        
        try {
            channel.rm(remoteTestFolderPath+propertyFileName);
            System.out.println("File on the target directory found, deleting");            
        } catch (SftpException ex) {
            fail("File on the target directory not found, deleting skipped");
        }
        channel.disconnect();
        System.out.println("*** testExecute  success ***");                
        
        
        
    }

    
    
    private ChannelSftp openChannel(){
        ChannelSftp channel = null;
        DpSftpChannel dpSftpChannel;
        try {
            dpSftpChannel = new DpSftpChannel();
            channel = dpSftpChannel.getSftpChannel();
        } catch (JSchException | GeneralSecurityException | IOException ex) {
            fail(ex.getLocalizedMessage());
        }
        return channel;
    }
    
}