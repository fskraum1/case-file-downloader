/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package FTP;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;
import gui.PanelUpload;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.html.Option;
import main.CasefileDownloader;
import utils.FileUtil;
import properties.SettingsProperties;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author m.kraus
 */
public class UploadFieldsUpdaterTest {

    private static SettingsProperties settingsProperties =SettingsProperties.getInstance();
    private String testDirectoryName = "test";
    private String testSubdirectory = "testSubDir";
    private String masterTestDirectory = settingsProperties.getLocalPathRoot()+testDirectoryName;
    private String copiedFileName = settingsProperties.getPropertyFileName();
    private String pathOfFileToCopy = masterTestDirectory + "\\"+testSubdirectory+"\\"+copiedFileName;    
    private String rootOnDP = "/Customers All/EMEA/!SUPPORT";
    private String destinationFolderOnDp = rootOnDP+"/test/";
    private UploadFieldsUpdater instance;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        System.out.println("****** setUp of UploadFieldsUpdaterTest ******");
        //check if the file exists, if yes then fail
        if(new File(masterTestDirectory).exists()){
            fail("Folder "+masterTestDirectory+" already exists. Loss of data could occur if continue.");
        }
        
        //creating the directory on the test folder
        FileUtil.createDirectories(new File(pathOfFileToCopy).getParent());
        
        // upload file to test directory        
        try {
            Files.copy(Paths.get(settingsProperties.getPropertyFileName()), Paths.get(pathOfFileToCopy), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ex) {
            ex.printStackTrace();
            fail(ex.getLocalizedMessage());
        }
        
        //check if the directory exists on DP and if not create it
        ChannelSftp channelSftp = null;
        try {            
            DpSftpChannel dpSftpChannel = new DpSftpChannel();
             channelSftp = dpSftpChannel.getSftpChannel();
        } catch (JSchException | GeneralSecurityException | IOException ex) {
            fail(ex.getLocalizedMessage());
        }
        
        try {
            System.out.println("checking if folder exists on DP");
            channelSftp.cd(destinationFolderOnDp);
        } catch (SftpException ex) {
            try {
                System.out.println("does not exists, creating");
                channelSftp.mkdir(destinationFolderOnDp);
            } catch (SftpException ex1) {
                fail("Failed to create directory on DP "+destinationFolderOnDp + "Exception "+ex1.getLocalizedMessage());
            }
        }
        
        
        // create instance of the tested object
        try {       
            instance = new UploadFieldsUpdater(new File(pathOfFileToCopy), new PanelUpload(CasefileDownloader.getProgressContainer()));
        } catch (JSchException | GeneralSecurityException | IOException ex) {
            fail(ex.getLocalizedMessage());
        }
        
    }

    @After
    public void tearDown() {
        try {
            // Remove everything from the master test directory recursive
            FileUtil.deleteRecursive(new File(masterTestDirectory));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Test of getTargetTicketDirectory method, of class UploadFieldsUpdater.
     */
    @Test
    public void testGetTargetTicketDirectory() {
        System.out.println("getTargetTicketDirectory");        
        String expResult = testDirectoryName+"/"+testSubdirectory;
        String result = instance.getTargetTicketDirectory();
        assertEquals(expResult, result);
    }

    /**
     * Test of getTargetFileName method, of class UploadFieldsUpdater.
     */
    @Test
    public void testGetTargetFileName() {
        System.out.println("getTargetFileName");
        String expResult = copiedFileName;
        String result = instance.getTargetFileName();
        assertEquals(expResult, result);
    }
    
    /**
     * Test of getTargetFileName method, of class UploadFieldsUpdater.
     */
    @Test
    public void testGet() {
        System.out.println("getTargetFileName");
        String expResult = rootOnDP;
        String result = null;
        try {
            instance.execute();
            result = instance.get();
        } catch (InterruptedException | ExecutionException ex) {
            
            fail("Exception raised durring calling of the get() method of the UploadFildsUpdater" + ex);            
        }
        assertEquals(expResult, result);
    }    

}