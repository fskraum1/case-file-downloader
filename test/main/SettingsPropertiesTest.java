/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import properties.SettingsProperties;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author m.kraus
 */
public class SettingsPropertiesTest {
    private SettingsProperties instance;
    private String backupSettingsFileName = "settings.backup";
    private String propertyFileName = SettingsProperties.getPropertyFileName();
    public SettingsPropertiesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {        
        File propertyFile = new File(propertyFileName);
        if(propertyFile.exists()){
            try {
                Files.move(Paths.get(propertyFileName), Paths.get(backupSettingsFileName), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException ex) {
                fail("error in moving settings file to backup location");
                ex.printStackTrace();                
                Logger.getLogger(SettingsPropertiesTest.class.getName()).log(Level.SEVERE, null, ex);
            }
            propertyFile.delete();
        }
        instance = SettingsProperties.getInstance();
    }
    
    @After
    public void tearDown() {
        try {
            Files.move(Paths.get(backupSettingsFileName), Paths.get(propertyFileName), StandardCopyOption.REPLACE_EXISTING);
        } 
        catch (IOException ex) {
            fail("error in moving settings file to backup location");
            ex.printStackTrace();
            Logger.getLogger(SettingsPropertiesTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        new File(backupSettingsFileName).delete();
    }

    /**
     * Test of getInstance method, of class SettingsProperties.
     */
    @Test
    public void testGetInstance() {
        System.out.println("getInstance");
        SettingsProperties expResult = instance;
        SettingsProperties result = SettingsProperties.getInstance();
        assertEquals("Instance returned is not the same as the one created in the test initialization", expResult, result);
    }

    /**
     * Test of isCredentialsEmpty method, of class SettingsProperties.
     */
    @Test
    public void testIsCredentialsEmpty() {
        System.out.println("isCredentialsEmpty");
        Boolean expResult = true;
        Boolean result = instance.isCredentialsEmpty();
        assertEquals("Credentials are not empty even in the beginning of the test they should be", expResult, result);
        
        expResult = false;
        instance.setUserName("sampleUser");
        try {
            instance.setPassword(new Password().encrypt("samplePassword"));
        } catch (GeneralSecurityException | UnsupportedEncodingException ex) {
            fail("problem in encrypting and setting password "+ex);
        }
        result = instance.isCredentialsEmpty();
        assertEquals("credentials returned as not empty even they have been manualy set",expResult, result);
        
    }

    /**
     * Test of login, of class SettingsProperties.
     */
    @Test
    public void testLogin() {
        System.out.println("login");
        String userName = "sampleUser";
        String password = "samplePassword&^6";
        
        instance.setUserName(userName);
        try {
            instance.setPassword(new Password().encrypt(password));
        } catch (GeneralSecurityException | UnsupportedEncodingException ex) {
            fail("problem in encrypting and setting password. "+ex);
        }
        
        String expResult = password;
        String result = null;
        try {
            result = new Password().decrypt(instance.getPassword());
        } catch (GeneralSecurityException | IOException ex) {
            fail("problem in decrypting password. "+ex);
        }
        assertEquals("password returned is not the same as inserted", expResult, result);
        
        expResult = userName;
        result = instance.getUsername();
        assertEquals("username returned is not the same as inserted", expResult, result);
        

    }

    /**
     * Test of getPortDataPoint method, of class SettingsProperties.
     */
    @Test
    public void testPortDataPoint() {
        System.out.println("PortDataPoint");
        int port = 3521;
        instance.setPortDataPoint(String.valueOf(port));        
        int expResult = port;
        int result = Integer.valueOf(instance.getPortDataPoint());
        assertEquals("Port obtained from settings properties is not the same as the inserted one",expResult, result);
    }

    /**
     * Test of getHostNameDataPoint method, of class SettingsProperties.
     */
    @Test
    public void testHostNameDataPoint() {
        System.out.println("HostNameDataPoint");
        String hostname = "sampleHostName.something.hk";
        String expResult = hostname;
        instance.setHostNameDataPoint(hostname);
        String result = instance.getHostNameDataPoint();
        assertEquals("Hostname placed in the setting properties is not the same as returned one",expResult, result);
    }


    /**
     * Test of getLocalPathRoot method, of class SettingsProperties.
     */
    @Test
    public void testGetLocalPathRoot() {
        System.out.println("getLocalPathRoot");
        String localPathRoot = "Z:\\someDirectory\\some subdirectory with space\\SF";
        String expResult = localPathRoot;
        instance.setLocalPathRoot(localPathRoot);
        String result = instance.getLocalPathRoot();
        assertEquals("local path to the root placed in the setting properties is not the same as returned one", expResult, result);
    }


    /**
     * Test of getLastUploadDirectory method, of class SettingsProperties.
     */
    @Test
    public void testGetLastUploadDirectory() {
        System.out.println("getLastUploadDirectory");
        File lastUploadDirectory = new File("d:\\něco\\subdirectory with space\\something more");
        File expResult = lastUploadDirectory;
        instance.setLastUploadDirectory(lastUploadDirectory);
        File result = instance.getLastUploadDirectory();
        assertEquals("Last upload directory placed in the setting properties is not the same as returned one", expResult, result);
    }
}