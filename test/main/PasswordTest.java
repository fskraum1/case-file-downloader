/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author m.kraus
 */
public class PasswordTest {
    
    public PasswordTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of encrypt and decrypt method, of class Password.
     */
    @Test
    public void testEncryptDecrypt() throws Exception {
        System.out.println("encrypt");
        String plainTextPassword = "PasswordToBeEncrypted";
        Password instance = new Password();
        String cryptedPassword = instance.encrypt(plainTextPassword);
        assertEquals(plainTextPassword, instance.decrypt(cryptedPassword));
        System.out.println("encrypt successful");
    }
}