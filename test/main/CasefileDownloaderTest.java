/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import properties.SettingsProperties;
import utils.FileUtil;
import FTP.SftpUploader;
import com.jcraft.jsch.JSchException;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author m.kraus
 */
public class CasefileDownloaderTest {
    private static CasefileDownloader instance;
    private static SettingsProperties settingsProperties;
    private String testDirectoryName = "test";
    
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        System.out.println("****** setUp of CasefileDownloaderTest ******");
        instance = (CasefileDownloader)CasefileDownloader.getProgressContainer();
        settingsProperties = SettingsProperties.getInstance();
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getInstance method, of class CasefileDownloader.
     */
    @Test
    public void testGetInstance() {
        System.out.println("*** getInstance ***");        
        CasefileDownloader result = (CasefileDownloader)CasefileDownloader.getProgressContainer();
        assertEquals(instance, result);
        System.out.println("*** getInstance successful ***"); 
    }

    
    
    @Test
    public void testUploadDownloadTicket(){
        System.out.println("*** UploadDownloadTicket ***");
        System.out.println("Checking expected state before test ...");
        File testDirectoryLocal = new File(settingsProperties.getLocalPathRoot()+testDirectoryName);
        if(testDirectoryLocal.exists()){
            fail("Directory on local path "+settingsProperties.getLocalPathRoot()+testDirectoryName+
                    " already exists. If the test would continue lose of data could occur");
        }
        System.out.println("done");
        System.out.println("Initiate upload"); 
        String source=settingsProperties.getPropertyFileName();
        String destinationFolder = "/Customers All/EMEA/!SUPPORT/test/";
        //TODO> before upload to the folder the existence of such file should be
        //checked and in case file exists delete it so the overwrite dialog is not initiated
        try {
            new SftpUploader(source,destinationFolder+source).execute();
        } catch (JSchException | GeneralSecurityException | IOException ex) {
            fail(ex.getLocalizedMessage());
        }
        
        System.out.println("Upload finished"); 

        instance.testingSetTextFieldTicketId("test");
        instance.testingButtonDownloadActionPerformed();
        try {
            Thread.sleep(5000);         //this is very wrong
        } catch (InterruptedException ex) {
            Logger.getLogger(CasefileDownloaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        assertTrue("File has not been downloaded", new File(settingsProperties.getLocalPathRoot()+"test\\"+settingsProperties.getPropertyFileName()).exists());
        File downloadedFolder = new File(settingsProperties.getLocalPathRoot()+"test\\");
        try {
            FileUtil.deleteRecursive(downloadedFolder);
        } 
        catch (IOException ex) {
            fail(ex.getLocalizedMessage());
        }
        
        System.out.println("*** UploadDownloadTicket successful ***"); 
    }    
}