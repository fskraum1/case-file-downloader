/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import utils.FileUtil;
import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author m.kraus
 */
public class FileUtilTest {
    
    public FileUtilTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createDirectories method, of class FileUtil.
     */
    @Test
    public void testCreateDirectories() {
        System.out.println("createDirectories");
        String directoryToCreate = "c:\\Temp\\casefileDownloaderJunitTest\\";
        String subDirectoryToCreate = directoryToCreate+"AnoteherDirectory\\";
        File testDirectory = new File(directoryToCreate);
        File testSubDirectory = new File(subDirectoryToCreate);
        if(testDirectory.exists()){
            fail("Directory to be created during test already exists. If the test would be "
                    + "initiated data loss could occur, please check content of the "
                    + directoryToCreate +" directory");
        }
        FileUtil.createDirectories(directoryToCreate);
        assertTrue("Directory has not been created", testDirectory.exists());
        
        FileUtil.createDirectories(subDirectoryToCreate);
        assertTrue("Directory has not been created", testSubDirectory.exists());               
                
        testSubDirectory.delete();
        testDirectory.delete();
        System.out.println("createDirectories succesfull");
        
    }

    /**
     * Test of getNewName method, of class FileUtil.
     */
    @Test
    public void testGetNewName() {
        System.out.println("getNewName");
        String originalName = "package.pkg";
        String expResult = "package-Copy.pkg";
        String result = FileUtil.getNewName(originalName);
        assertEquals(expResult, result);
        
        originalName = "package.pkg.ext";
        expResult = "package.pkg-Copy.ext";
        result = FileUtil.getNewName(originalName);
        assertEquals(expResult, result);
        System.out.println("getNewName succesfull");

    }
    
    //TODO> test the recursive delete of directories
}